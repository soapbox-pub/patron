defmodule Patron.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :ap_id, :string
      add :stripe_cus_id, :string

      timestamps()
    end

    create unique_index(:users, [:ap_id])
    create unique_index(:users, [:stripe_cus_id])
  end
end
