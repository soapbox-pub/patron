'use strict';

document.querySelectorAll('.btngroup__btn').forEach(elem => {
  elem.addEventListener('click', ({ target: btn }) => {
    const btngroup = btn.parentElement;
    const btngroup_sets = document.querySelector(btngroup.dataset.sets);
    const btngroup_shows = document.querySelector(btngroup.dataset.shows);

    const btn_shows = document.querySelector(btn.dataset.shows);
    const btn_hides = document.querySelector(btn.dataset.hides);
    const btn_focuses = document.querySelector(btn.dataset.focuses);
    const btn_value = btn.dataset.value;

    // Reset other button states
    btngroup.querySelectorAll('.btngroup__btn').forEach((other_btn) => {
      const other_btn_shows = document.querySelector(other_btn.dataset.shows);
      const other_btn_hides = document.querySelector(other_btn.dataset.hides);
      if (other_btn_shows) {
        other_btn_shows.style.display = 'none';
      }
      if (other_btn_hides) {
        other_btn_hides.style.display = '';
      }
      other_btn.classList.remove('btngroup__btn--active')
    });

    // Set given input
    if (btngroup_sets) {
      btngroup_sets.value = btn_value;
    }

    // Highlight current button
    btn.classList.add('btngroup__btn--active')

    // Set visibility of given elements
    if (btn_shows) {
      btn_shows.style.display = '';
    }
    if (btngroup_shows) {
      btngroup_shows.style.display = '';
    }
    if (btn_hides) {
      btn_hides.style.display = 'none';
    }

    // Focus given elements
    if (btn_focuses) {
      btn_focuses.focus();
    }

    return false; // Prevent form submit
  });
});

document.querySelector('.payform').addEventListener('submit', (e) => {
  e.preventDefault();
  document.getElementById("paybtn").disabled = true;

  const stripe_pk = document.querySelector("meta[name='stripe-pk']").content;
  const csrf_token = document.querySelector("meta[name='csrf-token']").content;
  const price = Math.floor(document.getElementById("price").value.replace(/[^0-9.]/, "") * 100);

  const stripe = Stripe(stripe_pk);
  const req = new XMLHttpRequest();

  function checkout () {
    stripe.redirectToCheckout({
      sessionId: this.responseText
    }).then(function (result) {
      console.log(result.error.message);
    });
  }

  req.addEventListener("load", checkout);
  req.open("POST", "/stripe_checkout");
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  req.setRequestHeader("X-CSRF-Token", csrf_token);
  req.send("amount=" + price);
});
