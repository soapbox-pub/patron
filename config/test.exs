import Config

config :patron,
  homeserver_baseurl: "https://pleroma.test",
  brand_name: "Test Site",
  brand_logo: "/images/phoenix.png",
  goal_amount: 100_00,
  goal_text: "Help pay for this project!",
  app_client_id: "ml7KsI0MtbG405o3pV_g2hvObenzGNZNGDn13yITdzQ",
  app_client_secret: "4lmpjxiEbrqZkh1xtb01aunmanrDXzNO0ahoX8MQZOk",
  http_client: HTTPoisonMock

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :patron, Patron.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "patron_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :patron, PatronWeb.Endpoint,
  http: [port: 4002],
  server: false,
  secret_key_base: "UUKDw13RgoCvMhlGaTG59ILalDsVZaUsNuPdJ96DuNNj7f//4H1RrYcrXRP8+lju",
  signing_salt: "01d4gbgL",
  live_view: [signing_salt: "k7ZBluD1"]

# Print only warnings and errors during test
config :logger, level: :warn

# Stripe
config :stripity_stripe,
  http_module: StripeApiMock,
  log_level: :debug
