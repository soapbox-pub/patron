# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :patron,
  ecto_repos: [Patron.Repo],
  brand_color: "#0482d8",
  http_client: HTTPoison,
  stripe_plan_id: "plan_monthly_donation"

# Configures the endpoint
config :patron, PatronWeb.Endpoint,
  url: [host: "localhost"],
  http: [ip: {127, 0, 0, 1}, port: 3037],
  render_errors: [view: PatronWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Patron.PubSub

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Stripe
config :stripity_stripe,
  api_key: fn -> Application.get_env(:patron, :stripe_secret_key) end

# Tesla
config :tesla, adapter: Tesla.Adapter.Hackney

# Ueberauth
config :ueberauth, Ueberauth,
  providers: [
    fediverse: {Ueberauth.Strategy.Mastodon, []}
  ]

# CORS
# https://hexdocs.pm/cors_plug/readme.html
config :cors_plug,
  origin: ["*"]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Dynamically import secrets, if exists
if File.exists?("./config/#{Mix.env()}.secret.exs"),
  do: import_config("#{Mix.env()}.secret.exs")
