defmodule PatronWeb.AuthControllerTest do
  use PatronWeb.ConnCase

  @session_opts [
    store: :cookie,
    key: "_test",
    signing_salt: "cooldude"
  ]

  # test "GET /auth/fediverse/callback", %{conn: conn} do
  #   token_data =
  #     Jason.decode!(
  #       File.read!(
  #         "test/fixtures/auth/oauth_token_LqHawX_Zf-MAVOhzpBWvCjs1MNkOH5Arq3aqvRTH7jE.json"
  #       )
  #     )
  #
  #   conn =
  #     conn
  #     |> Plug.Session.call(Plug.Session.init(@session_opts))
  #     |> fetch_session()
  #     |> get("/auth/fediverse/callback", %{
  #       "code" => "LqHawX_Zf-MAVOhzpBWvCjs1MNkOH5Arq3aqvRTH7jE"
  #     })
  #
  #   assert get_session(conn, :token_data) == token_data
  #   assert redirected_to(conn) == "/"
  # end

  test "GET /auth/logout", %{conn: conn} do
    token_data =
      Jason.decode!(
        File.read!(
          "test/fixtures/auth/oauth_token_LqHawX_Zf-MAVOhzpBWvCjs1MNkOH5Arq3aqvRTH7jE.json"
        )
      )

    conn =
      conn
      |> Plug.Session.call(Plug.Session.init(@session_opts))
      |> fetch_session()
      |> put_session(:token_data, token_data)
      |> get("/auth/logout")

    assert get_session(conn, :token_data) == nil
    assert redirected_to(conn) == "/"
  end
end
