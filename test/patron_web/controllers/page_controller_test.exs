defmodule PatronWeb.PageControllerTest do
  use PatronWeb.ConnCase
  alias Patron.User
  alias Patron.Repo

  @session_opts [
    store: :cookie,
    key: "_test",
    signing_salt: "cooldude"
  ]

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Donate"
  end

  test "POST /stripe_checkout while logged in", %{conn: conn} do
    user_data =
      Jason.decode!(
        File.read!(
          "test/fixtures/auth/verify_credentials_Ex6S1gvZIVIn2d0jzIGvRY9mlqC31ccGjRZpYMlZL2A.json"
        )
      )

    {:ok, _} =
      %User{ap_id: "https://gleasonator.com/users/alex", stripe_cus_id: "cus_HUTSv6srWKdT7y"}
      |> Ecto.Changeset.change()
      |> Repo.insert()

    conn =
      conn
      |> Plug.Session.call(Plug.Session.init(@session_opts))
      |> fetch_session()
      |> assign(:user_data, user_data)
      |> post("/stripe_checkout", %{amount: 1200})

    assert text_response(conn, 200) == "cs_EsOSyeoStCxpP4E"
  end

  test "GET / with invalid auth cookie", %{conn: conn} do
    conn =
      conn
      |> Plug.Session.call(Plug.Session.init(@session_opts))
      |> fetch_session()
      |> put_session(:token_data, %{"access_token" => "invalid-token"})
      |> get("/")

    assert html_response(conn, 200) =~ "Donate"
  end

  # test "GET / while being a donor", %{conn: conn} do
  #   token_data =
  #     Jason.decode!(
  #       File.read!(
  #         "test/fixtures/auth/oauth_token_LqHawX_Zf-MAVOhzpBWvCjs1MNkOH5Arq3aqvRTH7jE.json"
  #       )
  #     )
  #
  #   {:ok, _user} =
  #     %Patron.User{
  #       ap_id: "https://gleasonator.com/users/alex",
  #       stripe_cus_id: "cus_HUTSv6srWKdT7y"
  #     }
  #     |> Ecto.Changeset.change()
  #     |> Patron.Repo.insert()
  #
  #   conn =
  #     conn
  #     |> Plug.Session.call(Plug.Session.init(@session_opts))
  #     |> fetch_session()
  #     |> put_session(:token_data, token_data)
  #     |> get("/")
  #
  #   assert html_response(conn, 200) =~ "Your donation"
  # end
end
