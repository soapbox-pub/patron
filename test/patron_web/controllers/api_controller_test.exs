defmodule PatronWeb.ApiControllerTest do
  use PatronWeb.ConnCase

  test "GET /api/patron/v1/instance", %{conn: conn} do
    conn = get(conn, "/api/patron/v1/instance")

    assert json_response(conn, 200) == %{
             "url" => "http://localhost:4002",
             "funding" => %{
               "amount" => 9520,
               "patrons" => 3,
               "currency" => "usd",
               "interval" => "monthly"
             },
             "goals" => [
               %{
                 "amount" => 10000,
                 "currency" => "usd",
                 "interval" => "monthly",
                 "text" => "Help pay for this project!"
               }
             ]
           }
  end

  test "GET /api/patron/v1/accounts/:ap_id", %{conn: conn} do
    {:ok, _user} = Patron.User.create_from_ap_id("https://hyrule.world/users/link")
    conn = get(conn, "/api/patron/v1/accounts/https%3A%2F%2Fhyrule.world%2Fusers%2Flink")

    assert json_response(conn, 200) == %{
             "url" => "https://hyrule.world/users/link",
             "is_patron" => true
           }
  end
end
