defmodule PatronWeb.PlanControllerTest do
  use PatronWeb.ConnCase

  describe "GET /plan/:id/edit" do
    test "returns HTTP 400 for logged out user", %{conn: conn} do
      conn = get(conn, "/plan/sub_HYa6soT4GQftPd/edit")
      assert response(conn, 400)
    end

    test "returns HTTP 400 for unauthorized user", %{conn: conn} do
      user_subs =
        Stripe.Converter.convert_result(
          Jason.decode!(File.read!("test/fixtures/stripe/subscriptions_list_hyrule.json"))
        )

      conn =
        conn
        |> assign(:user_subs, user_subs)
        |> get("/plan/sub_HYa6soT4GQftPd/edit")

      assert response(conn, 400)
    end

    test "displays the edit form for authorized user", %{conn: conn} do
      user_subs =
        Stripe.Converter.convert_result(
          Jason.decode!(File.read!("test/fixtures/stripe/subscriptions_list_hyrule.json"))
        )

      conn =
        conn
        |> assign(:user_subs, user_subs)
        |> get("/plan/sub_HUTTWoOkloYoMn/edit")

      assert html_response(conn, 200) =~ "Edit plan"
    end
  end

  describe "POST /plan/:id/edit" do
    test "updates the plan", %{conn: conn} do
      user_subs =
        Stripe.Converter.convert_result(
          Jason.decode!(File.read!("test/fixtures/stripe/subscriptions_list_hyrule.json"))
        )

      conn =
        conn
        |> assign(:user_subs, user_subs)
        |> post("/plan/sub_HUTTWoOkloYoMn/edit", %{amount: "10000"})

      assert redirected_to(conn) == "/"
    end
  end

  describe "GET /plan/:id/cancel" do
    test "shows the confirmation form", %{conn: conn} do
      user_subs =
        Stripe.Converter.convert_result(
          Jason.decode!(File.read!("test/fixtures/stripe/subscriptions_list_hyrule.json"))
        )

      conn =
        conn
        |> assign(:user_subs, user_subs)
        |> get("/plan/sub_HUTTWoOkloYoMn/cancel")

      assert html_response(conn, 200) =~ "Cancel plan"
    end
  end

  describe "POST /plan/:id/cancel" do
    test "cancels the plan", %{conn: conn} do
      user_subs =
        Stripe.Converter.convert_result(
          Jason.decode!(File.read!("test/fixtures/stripe/subscriptions_list_alex_1.json"))
        )

      conn =
        conn
        |> assign(:user_subs, user_subs)
        |> post("/plan/sub_HYwyn7OOpVlSgF/cancel")

      assert redirected_to(conn) == "/"
    end

    test "cancels the plan with refund", %{conn: conn} do
      user_subs =
        Stripe.Converter.convert_result(
          Jason.decode!(File.read!("test/fixtures/stripe/subscriptions_list_alex_2.json"))
        )

      conn =
        conn
        |> assign(:user_subs, user_subs)
        |> post("/plan/sub_HYx7i3dAb5R350/cancel", %{refund: "on"})

      assert redirected_to(conn) == "/"
    end
  end
end
