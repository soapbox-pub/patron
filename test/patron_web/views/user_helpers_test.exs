defmodule PatronWeb.UserHelpersTest do
  use PatronWeb.ConnCase
  import PatronWeb.UserHelpers
  alias Patron.User
  alias Patron.Repo

  @session_opts [
    store: :cookie,
    key: "_test",
    signing_salt: "cooldude"
  ]

  test "has_user/1", %{conn: conn} do
    user_data =
      Jason.decode!(
        File.read!(
          "test/fixtures/auth/verify_credentials_Ex6S1gvZIVIn2d0jzIGvRY9mlqC31ccGjRZpYMlZL2A.json"
        )
      )

    conn =
      conn
      |> Plug.Session.call(Plug.Session.init(@session_opts))
      |> fetch_session()
      |> assign(:user_data, user_data)

    assert has_user(conn) == true
  end

  test "user_data/2", %{conn: conn} do
    token_data =
      Jason.decode!(
        File.read!(
          "test/fixtures/auth/verify_credentials_Ex6S1gvZIVIn2d0jzIGvRY9mlqC31ccGjRZpYMlZL2A.json"
        )
      )

    {:ok, _user} =
      %User{ap_id: "https://gleasonator.com/users/alex"}
      |> Ecto.Changeset.change()
      |> Repo.insert()

    conn =
      conn
      |> Plug.Session.call(Plug.Session.init(@session_opts))
      |> fetch_session()
      |> put_session(:token_data, token_data)
      |> assign(
        :user_data,
        token_data
      )

    assert user_data(conn, ["acct"]) == "alex"
    assert user_data(conn, [:acct]) == "alex"

    assert user_data(conn, ["avatar"]) ==
             "https://media.gleasonator.com/accounts/avatars/000/000/001/original/1a630e4c4c64c948.jpg"

    assert user_data(conn, [:pleroma, :settings_store, :soapbox_fe, :autoPlayGif]) == true
    assert user_data(conn, [:fields, 0, :name]) == "Website"
  end

  test "is_patron/1", %{conn: conn} do
    assert is_patron(conn) == false

    conn = assign(conn, :user_subs, %Stripe.List{data: []})
    assert is_patron(conn) == false

    conn = assign(conn, :user_subs, %Stripe.List{data: [1, 2, 3]})
    assert is_patron(conn) == true
  end
end
