defmodule PatronWeb.Plugs.VerifySubOwnerTest do
  use ExUnit.Case, async: true
  use Plug.Test

  alias PatronWeb.Plugs.VerifySubOwner

  test "returns unauthorized by default" do
    # Create a test connection
    conn = conn(:get, "/")

    # Invoke the plug
    conn = VerifySubOwner.call(conn, nil)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 400
  end

  test "authorizes when the user owns the subscription" do
    user_subs =
      Stripe.Converter.convert_result(
        Jason.decode!(File.read!("test/fixtures/stripe/subscriptions_list_hyrule.json"))
      )

    # Create a test connection
    conn =
      conn(:get, "/", %{"id" => "sub_HUTTWoOkloYoMn"})
      |> assign(:user_subs, user_subs)
      |> VerifySubOwner.call(nil)

    # Assert the response and status
    assert conn.state == :unset
    assert conn.status == nil
  end
end
