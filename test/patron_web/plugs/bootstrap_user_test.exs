defmodule PatronWeb.Plugs.BootstrapUserTest do
  use ExUnit.Case, async: true
  use Plug.Test
  alias PatronWeb.Plugs.BootstrapUser

  @session_opts [
    store: :cookie,
    key: "_test",
    signing_salt: "cooldude"
  ]

  test "default behavior" do
    # Create a test connection
    conn =
      conn(:get, "/")
      |> Plug.Session.call(Plug.Session.init(@session_opts))
      |> fetch_session()
      |> BootstrapUser.call(nil)

    # Assert the response and status
    assert conn.state == :unset
    assert conn.status == nil
  end
end
