defmodule StripeApiMock do
  def request(:get, "https://api.stripe.com/v1/subscriptions?plan=plan_monthly_donation", _, _, _) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/subscriptions_monthly_1.json")}
  end

  def request(:post, "https://api.stripe.com/v1/checkout/sessions", _, _, _) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/checkout_1.json")}
  end

  def request(
        :get,
        "https://api.stripe.com/v1/subscriptions?customer=cus_HUTSv6srWKdT7y&plan=plan_monthly_donation",
        _,
        _,
        _
      ) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/subscriptions_list_hyrule.json")}
  end

  def request(:post, "https://api.stripe.com/v1/customers", _, _, _) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/new_cus_HXv3XdJLsqXclW.json")}
  end

  def request(
        :post,
        "https://api.stripe.com/v1/subscriptions/sub_HUTTWoOkloYoMn",
        _,
        "items%5B0%5D%5Bid%5D=si_HUTT6C4aP60u2B&items%5B0%5D%5Bquantity%5D=1000000&prorate=false",
        _
      ) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/update_sub_HUTTWoOkloYoMn.json")}
  end

  def request(:delete, "https://api.stripe.com/v1/subscriptions/sub_HYwyn7OOpVlSgF", _, _, _) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/delete_sub_HYwyn7OOpVlSgF.json")}
  end

  def request(:delete, "https://api.stripe.com/v1/subscriptions/sub_HYx7i3dAb5R350", _, _, _) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/delete_sub_HYx7i3dAb5R350.json")}
  end

  def request(:get, "https://api.stripe.com/v1/invoices/in_1GzpDdAaUj2SBwe3oS5TxzqG", _, _, _) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/in_1GzpDdAaUj2SBwe3oS5TxzqG.json")}
  end

  def request(
        :post,
        "https://api.stripe.com/v1/refunds",
        _,
        "charge=ch_1GzpDeAaUj2SBwe3Um72zTMp",
        _
      ) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/refund_ch_1GzpDeAaUj2SBwe3Um72zTMp.json")}
  end

  def request(
        :get,
        "https://api.stripe.com/v1/subscriptions?customer=cus_HXv3XdJLsqXclW&plan=plan_monthly_donation",
        _,
        _,
        _
      ) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/subscriptions_list_hyrule.json")}
  end

  def request(
        :get,
        "https://api.stripe.com/v1/subscriptions?customer=cus_HUTMFW2XJhO6V0&plan=plan_monthly_donation",
        _,
        _,
        _
      ) do
    {:ok, 200, [], File.read!("test/fixtures/stripe/subscriptions_list_empty.json")}
  end
end
