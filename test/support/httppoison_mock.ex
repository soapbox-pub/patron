defmodule HTTPoisonMock do
  def post(
        "https://pleroma.test/oauth/token",
        "{\"client_id\":\"ml7KsI0MtbG405o3pV_g2hvObenzGNZNGDn13yITdzQ\",\"client_secret\":\"4lmpjxiEbrqZkh1xtb01aunmanrDXzNO0ahoX8MQZOk\",\"code\":\"LqHawX_Zf-MAVOhzpBWvCjs1MNkOH5Arq3aqvRTH7jE\",\"grant_type\":\"authorization_code\",\"redirect_uri\":\"urn:ietf:wg:oauth:2.0:oob\"}",
        "Content-Type": "application/json"
      ) do
    {:ok,
     %HTTPoison.Response{
       body:
         File.read!(
           "test/fixtures/auth/oauth_token_LqHawX_Zf-MAVOhzpBWvCjs1MNkOH5Arq3aqvRTH7jE.json"
         ),
       status_code: 200
     }}
  end

  def post(
        "https://pleroma.test/oauth/token",
        "{\"client_id\":\"ml7KsI0MtbG405o3pV_g2hvObenzGNZNGDn13yITdzQ\",\"client_secret\":\"4lmpjxiEbrqZkh1xtb01aunmanrDXzNO0ahoX8MQZOk\",\"code\":\"invalid-code\",\"grant_type\":\"authorization_code\",\"redirect_uri\":\"urn:ietf:wg:oauth:2.0:oob\"}",
        "Content-Type": "application/json"
      ) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/auth/oauth_token_invalid-code.json"),
       status_code: 403
     }}
  end

  def get("https://pleroma.test/api/v1/accounts/verify_credentials",
        Authorization: "Bearer Ex6S1gvZIVIn2d0jzIGvRY9mlqC31ccGjRZpYMlZL2A"
      ) do
    {:ok,
     %HTTPoison.Response{
       body:
         File.read!(
           "test/fixtures/auth/verify_credentials_Ex6S1gvZIVIn2d0jzIGvRY9mlqC31ccGjRZpYMlZL2A.json"
         ),
       status_code: 200
     }}
  end

  def get("https://pleroma.test/api/v1/accounts/verify_credentials",
        Authorization: "Bearer invalid-token"
      ) do
    {:error,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/auth/verify_credentials_invalid-token.json"),
       status_code: 400
     }}
  end
end
