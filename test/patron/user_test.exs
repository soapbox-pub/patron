defmodule Patron.UserTest do
  alias Patron.User
  alias Patron.Repo
  use PatronWeb.ConnCase

  describe "User.get_subscriptions" do
    test "returns the subscriptions" do
      {:ok, link} =
        %User{ap_id: "https://hyrule.world/users/link", stripe_cus_id: "cus_HUTSv6srWKdT7y"}
        |> Ecto.Changeset.change()
        |> Repo.insert()

      {:ok, %Stripe.List{data: data} = _subs} = User.get_subscriptions(link)
      assert Enum.count(data) == 1
      assert Enum.at(data, 0).object == "subscription"
    end

    test "returns nil when stripe_cus_id isn't set" do
      {:ok, user} =
        %User{ap_id: "https://example.tld/users/test"}
        |> Ecto.Changeset.change()
        |> Repo.insert()

      assert User.get_subscriptions(user) == {:ok, nil}
    end
  end

  test "User.get_from_ap_id/1" do
    {:ok, link} =
      %User{ap_id: "https://hyrule.world/users/link"}
      |> Ecto.Changeset.change()
      |> Repo.insert()

    assert User.get_from_ap_id("https://hyrule.world/users/link") == link
  end

  test "User.create_from_ap_id/1" do
    {:ok, user} = User.create_from_ap_id("https://hyrule.world/users/link")
    assert user.ap_id == "https://hyrule.world/users/link"
    assert user.stripe_cus_id == "cus_HXv3XdJLsqXclW"
  end

  describe "User.is_patron/1" do
    test "returns true when the user has subscriptions" do
      {:ok, user} = Patron.User.create_from_ap_id("https://hyrule.world/users/link")
      assert User.is_patron(user) == true
    end

    test "returns false with no subscriptions" do
      {:ok, user} =
        Patron.Repo.insert(
          Patron.User.changeset(%Patron.User{}, %{
            ap_id: "https://mushroom.kingdom/users/mario",
            stripe_cus_id: "cus_HUTMFW2XJhO6V0"
          })
        )

      assert User.is_patron(user) == false
    end
  end
end
