defmodule Patron.FundingTest do
  alias Patron.Funding
  use PatronWeb.ConnCase

  test "Funding.get_funding" do
    assert Funding.get_funding() == %Funding{
             amount: 9520,
             patrons: 3,
             currency: :usd,
             interval: :monthly
           }
  end
end
