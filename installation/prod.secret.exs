import Config

# Stripe
config :patron,
  homeserver_baseurl: "https://gleasonator.com",
  brand_color: "#1ca82b",
  brand_name: "Gleasonator",
  brand_logo: "https://gleasonator.com/instance/logo-white.png",
  goal_amount: 200_00,
  goal_text: "I'll be able to afford an avocado.",
  stripe_public_key: "pk_live_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  stripe_secret_key: "sk_live_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  app_client_id: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  app_client_secret: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

config :patron, PatronWeb.Endpoint,
  url: [host: "patron.gleasonator.com", scheme: "https", port: 443],
  http: [ip: {127, 0, 0, 1}, port: 3037],
  secret_key_base: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  signing_salt: "XXXXXXXX",
  live_view: [signing_salt: "XXXXXXXX"]

config :patron, Patron.Repo,
  password: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

# Ueberauth
config :ueberauth, Ueberauth,
  providers: [
    fediverse: {Ueberauth.Strategy.Mastodon, [
      # You MUST provide an instance.
      instance: "https://gleasonator.com",
      # You MUST provide app credentials.
      # Generate your app before getting started.
      client_id: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
      client_secret: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
      scope: "read:accounts write:statuses"
    ]}
  ]
