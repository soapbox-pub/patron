defmodule PatronWeb.Plugs.VerifySubOwner do
  import Phoenix.Controller, only: [text: 2]
  import Plug.Conn

  @behaviour Plug

  def init(_), do: nil

  def call(%Plug.Conn{assigns: %{user_subs: %{data: subs}}, params: %{"id" => id}} = conn, _) do
    case id in get_sub_ids(subs) do
      true -> conn
      false -> not_authorized(conn)
    end
  end

  def call(conn, _), do: not_authorized(conn)

  defp get_sub_ids(subs) do
    Enum.reduce(subs, [], fn sub, acc -> acc ++ [sub.id] end)
  end

  defp not_authorized(conn) do
    conn
    |> put_status(400)
    |> text("Not authorized")
    |> halt()
  end
end
