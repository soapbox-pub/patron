defmodule PatronWeb.Plugs.BootstrapUser do
  import Plug.Conn
  alias Patron.User
  alias Ueberauth.Auth.Credentials
  alias Ueberauth.Strategy.Mastodon

  @behaviour Plug
  @baseurl Application.get_env(:patron, :homeserver_baseurl)

  def init(_), do: nil

  def call(conn, _) do
    case get_session(conn, :token_data) do
      nil -> conn
      %{"access_token" => token} -> verify_token(conn, token)
      %Credentials{token: token} -> verify_token(conn, token)
      _ -> delete_session(conn, :token_data)
    end
  end

  defp verify_token(conn, token) do
    with {:ok, %{status: 200, body: %{"url" => ap_id} = data}} <-
           Mastodon.API.account_verify_credentials(@baseurl, token) do
      conn
      |> assign(:user_data, data)
      |> maybe_set_stripe_data(ap_id)
    else
      _ -> delete_session(conn, :token_data)
    end
  end

  defp maybe_set_stripe_data(conn, ap_id) do
    with %User{} = user <- User.get_from_ap_id(ap_id),
         {:ok, subs} <- User.get_subscriptions(user) do
      assign(conn, :user_subs, subs)
    else
      _ -> conn
    end
  end
end
