defmodule PatronWeb.UserHelpers do
  use PatronWeb, :controller

  def is_patron(conn) do
    case conn.assigns do
      %{user_subs: %{data: subs}} -> Enum.count(subs) > 0
      _ -> false
    end
  end

  def has_user(conn) do
    case conn.assigns do
      %{user_data: _subs} -> true
      _ -> false
    end
  end

  def user_data(conn, path) do
    %{user_data: data} = conn.assigns

    Enum.reduce(path, data, fn key, acc ->
      if is_list(acc), do: Enum.at(acc, key), else: acc[to_string(key)]
    end)
  end
end
