defmodule PatronWeb.PageController do
  use PatronWeb, :controller
  alias Patron.User

  def index(conn, _params), do: render(conn, "index.html")

  def index(conn), do: index(conn, %{})

  def stripe_checkout(conn, %{"amount" => amount} = _params) do
    base_url = PatronWeb.Endpoint.url()

    with %{user_data: %{"url" => ap_id}} <- conn.assigns,
         {:ok, %User{stripe_cus_id: stripe_cus_id}} <- User.get_or_create_from_ap_id(ap_id),
         {:ok, %{id: checkout_session_id}} <-
           Stripe.Session.create(%{
             customer: stripe_cus_id,
             payment_method_types: ["card"],
             subscription_data: %{
               items: [
                 %{
                   plan: Application.get_env(:patron, :stripe_plan_id),
                   quantity: amount
                 }
               ]
             },
             success_url: "#{base_url}/?session_id={CHECKOUT_SESSION_ID}",
             cancel_url: base_url
           }) do
      text(conn, checkout_session_id)
    end
  end
end
