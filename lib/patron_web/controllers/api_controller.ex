defmodule PatronWeb.ApiController do
  use PatronWeb, :controller
  alias Patron.Funding
  alias Patron.User

  @doc "GET /api/patron/v1/instance"
  def instance(conn, _params) do
    json(conn, %{
      url: PatronWeb.Endpoint.url(),
      funding: Funding.get_funding(),
      goals: [
        %{
          amount: Application.get_env(:patron, :goal_amount),
          currency: :usd,
          interval: :monthly,
          text: Application.get_env(:patron, :goal_text)
        }
      ]
    })
  end

  def account(conn, %{"ap_id" => ap_id}) do
    with %User{} = user <- User.get_from_ap_id(ap_id) do
      json(conn, %{
        url: user.ap_id,
        is_patron: User.is_patron(user)
      })
    else
      _ ->
        json(conn, %{
          url: ap_id,
          is_patron: false
        })
    end
  end
end
