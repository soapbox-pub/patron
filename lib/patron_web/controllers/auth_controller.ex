defmodule PatronWeb.AuthController do
  use PatronWeb, :controller

  alias Ueberauth.Auth
  alias Ueberauth.Auth.Credentials
  alias Ueberauth.Failure
  alias Ueberauth.Failure.Error

  def callback(
        %{assigns: %{ueberauth_auth: %Auth{credentials: %Credentials{} = credentials}}} = conn,
        _params
      ) do
    conn
    |> put_session(:token_data, credentials)
    |> redirect(to: "/")
  end

  def callback(
        %{assigns: %{ueberauth_failure: %Failure{errors: [%Error{message: message} | _]}}} = conn,
        _params
      ) do
    conn
    |> put_flash(:error, message)
    |> redirect(to: "/")
  end

  def callback(conn, _params) do
    conn
    |> put_flash(:error, "Failed to authenticate.")
    |> redirect(to: "/")
  end

  def logout(conn, _params) do
    conn
    |> delete_session(:token_data)
    |> put_flash(:info, "You've been successfully logged out.")
    |> redirect(to: "/")
  end
end
