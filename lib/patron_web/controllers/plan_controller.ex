defmodule PatronWeb.PlanController do
  use PatronWeb, :controller

  plug(PatronWeb.Plugs.VerifySubOwner)

  def edit_form(%Plug.Conn{assigns: %{user_subs: %{data: subs}}} = conn, %{"id" => id} = _params) do
    render(conn, "edit_form.html", %{sub: find_sub(subs, id), csrf_token: get_csrf_token()})
  end

  def edit(
        %Plug.Conn{assigns: %{user_subs: %{data: subs}}} = conn,
        %{"id" => id, "amount" => amount} = _params
      ) do
    sub = find_sub(subs, id)

    Stripe.Subscription.update(sub.id, %{
      prorate: false,
      items: [
        %{
          id: Enum.at(sub.items.data, 0).id,
          quantity: floor(elem(Float.parse(amount), 0) * 100)
        }
      ]
    })

    conn
    |> put_flash(:info, "Your plan has been updated.")
    |> redirect(to: "/")
  end

  def cancel_form(
        %Plug.Conn{assigns: %{user_subs: %{data: subs}}} = conn,
        %{"id" => id} = _params
      ) do
    render(conn, "cancel_form.html", %{sub: find_sub(subs, id), csrf_token: get_csrf_token()})
  end

  def cancel(
        %Plug.Conn{assigns: %{user_subs: %{data: _subs}}} = conn,
        %{"id" => id, "refund" => _} = _params
      ) do
    with {:ok, sub} <- Stripe.Subscription.delete(id),
         {:ok, invoice} <- Stripe.Invoice.retrieve(sub.latest_invoice),
         {:ok, refund} <- Stripe.Refund.create(%{charge: invoice.charge}),
         amount = CurrencyFormatter.format(refund.amount, :usd) do
      conn
      |> put_flash(:info, "Your plan has been cancelled. Your card will be refunded #{amount}.")
      |> redirect(to: "/")
    end
  end

  def cancel(%Plug.Conn{assigns: %{user_subs: %{data: _subs}}} = conn, %{"id" => id} = _params) do
    Stripe.Subscription.delete(id)

    conn
    |> put_flash(:info, "Your plan has been cancelled.")
    |> redirect(to: "/")
  end

  defp find_sub(subs, id) do
    Enum.find(subs, fn sub -> sub.id == id end)
  end
end
