defmodule PatronWeb.Router do
  use PatronWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_layout, {PatronWeb.LayoutView, "columns.html"}
    plug PatronWeb.Plugs.BootstrapUser
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PatronWeb do
    pipe_through :browser

    get "/", PageController, :index
    post "/stripe_checkout", PageController, :stripe_checkout
  end

  scope "/auth", PatronWeb do
    pipe_through [:browser, Ueberauth]

    get "/fediverse", AuthController, :request
    get "/fediverse/callback", AuthController, :callback

    get "/logout", AuthController, :logout
  end

  scope "/plan", PatronWeb do
    pipe_through :browser

    get "/:id/edit", PlanController, :edit_form
    post "/:id/edit", PlanController, :edit
    get "/:id/cancel", PlanController, :cancel_form
    post "/:id/cancel", PlanController, :cancel
  end

  scope "/api/patron/v1", PatronWeb do
    pipe_through :api

    get "/instance", ApiController, :instance
    get "/accounts/:ap_id", ApiController, :account
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: PatronWeb.Telemetry
    end
  end
end
