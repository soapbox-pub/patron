defmodule Patron.Repo do
  use Ecto.Repo,
    otp_app: :patron,
    adapter: Ecto.Adapters.Postgres
end
