defmodule Patron.Funding do
  alias Patron.Funding

  @derive {Jason.Encoder, only: [:amount, :patrons, :currency, :interval]}
  defstruct amount: 0, patrons: 0, currency: :usd, interval: :monthly

  def get_funding() do
    with subs when is_list(subs) <- fetch_all_subs() do
      %Funding{
        amount: Enum.reduce(subs, 0, fn sub, acc -> acc + sub.quantity end),
        patrons: length(subs)
      }
    else
      {:error, _} -> %Funding{}
    end
  end

  def fetch_all_subs() do
    with {:ok, %Stripe.List{data: data} = list} <- Stripe.Subscription.list(%{plan: plan()}) do
      paginate_subs(list, data)
    end
  end

  defp paginate_subs(%Stripe.List{has_more: true, data: data} = _list, acc)
       when is_list(acc) and is_list(data) do
    with %{id: last_id} = List.last(data),
         {:ok, %{data: subs} = list} <-
           Stripe.Subscription.list(%{plan: plan(), starting_after: last_id}) do
      paginate_subs(list, acc ++ subs)
    end
  end

  defp paginate_subs(%Stripe.List{} = _list, acc) when is_list(acc), do: acc

  defp plan(), do: Application.get_env(:patron, :stripe_plan_id)
end
