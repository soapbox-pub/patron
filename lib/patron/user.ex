defmodule Patron.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Patron.Repo
  alias Patron.User

  schema "users" do
    field :ap_id, :string
    field :stripe_cus_id, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:ap_id, :stripe_cus_id])
    |> validate_required([:ap_id])
    |> unique_constraint([:ap_id, :stripe_cus_id])
  end

  def get_from_ap_id(ap_id) do
    Repo.get_by(User, ap_id: ap_id)
  end

  def create_from_ap_id(ap_id) do
    with {:ok, %Stripe.Customer{id: cus_id}} <- Stripe.Customer.create(%{}),
         {:ok, %User{} = user} <-
           Repo.insert(changeset(%User{}, %{ap_id: ap_id, stripe_cus_id: cus_id})) do
      {:ok, user}
    end
  end

  def get_or_create_from_ap_id(ap_id) do
    with %User{} = user <- get_from_ap_id(ap_id) do
      {:ok, user}
    else
      _ -> create_from_ap_id(ap_id)
    end
  end

  def get_subscriptions(user) do
    case user.stripe_cus_id do
      nil ->
        {:ok, nil}

      cus_id ->
        Stripe.Subscription.list(%{
          customer: cus_id,
          plan: Application.get_env(:patron, :stripe_plan_id)
        })
    end
  end

  def is_patron(user) do
    with {:ok, %Stripe.List{data: subs}} <- get_subscriptions(user) do
      Enum.count(subs) > 0
    else
      _ -> false
    end
  end
end
